<?php

namespace App\Http\Controllers;
use App\Book;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        $user = User::find($id);
        $books = $user->books;
         return view('books.index',  compact('books'));
       
        // //get all books 
        // $books = Book::all();
        // return view('books.index',['books'=>$books]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
        public function create()
        {   
            if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
            return view ('books.create');
        }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
          if (Gate::denies('manager')) {
        abort(403,"Are you a hacker or what?");
   } 
        $this->validate($request,[
            'title'=>'required',
            'author'=>'required']);
            
                   
                   $book = new Book();
                   $id = Auth::id();//show book only fot logged
                   $book->title = $request->title;
                   $book->user_id = $id;
                   $book->status = 0;
                   $book ->author=$request->author;
                   $book->created_at =now()->toDateTimeString();;
                   $book->save();
                   return redirect('books'); 
                   
                   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {     if (Gate::denies('manager')) {
        abort(403,"Are you a hacker or what?");
   }
          $book = Book::find($id);
        return view('books.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit books..");
        }

             
        if (!$book->user->id == Auth::id()){
            return (readdirect('books'));
        }

        if (!$request->ajax())
        {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'author' => 'required',
            ]);
            
            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
            $book->update($request->except(['_token']));
        } 
            else 
            {
                $book->update($request->except(['_token']));
                return Response::json(array('result'=>'success', 'status'=>$request->status),200);
            }
            
            return redirect('books');

        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        if(!$book->user->id ==Auth::id()) 
        {
            return (redirect('books'));
        }

        $book ->delete();
        return redirect('books');
    }
    
}
