<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'maya',
                    'email' => 'maya@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                    'name' => 'shir',
                    'email' => 'shir@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Alon',
                    'email' => 'Alon@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Ori',
                    'email' => 'Ori@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);

        
            }
}
