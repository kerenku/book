<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
        [
            [
                
                'user_id'=>6,
                'title' => 'Harry Poter',
                'author' => 'J.K Rolling',
                'created_at' => date('Y-m-d G:i:s'),    
            ],
            [
                
                'user_id'=>6,
                'title' => 'Game of Thrones',
                'author' => 'George Raymond Richard Martin',
                'created_at' => date('Y-m-d G:i:s'),  
            ],
            [
                
                'title' => 'Rich dad, Poor dad',
                'user_id'=>6,
                'author' => 'Robert Kiyosaki',
                'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                
                'title' => 'Asymmetry',
                'user_id'=>7,
                'author' => 'Lisa Halliday',
                'created_at' => date('Y-m-d G:i:s'),    
            ],
            [
                
                'title' => 'Educated: A Memoir',
                'user_id'=>7,
                'author' => 'Tara Westover',
                'created_at' => date('Y-m-d G:i:s'),
            ],    
        ]);
    }
}